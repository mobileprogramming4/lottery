void main() {
  isPrime(4);
  isPrime(5);
  lottery("123456", "123456");
  lottery("123456", "654321");
}

bool isPrime(int number) {
  for (var i = 2; i <= number / i; i++) {
    if (number % i == 0) {
      print('$number is a prime number.');
      return false;
    }
  }
  print('$number is not a prime number.');
  return true;
}

void lottery(var ans, var number) {
  if (ans == number) {
    print("You won the lottery.");
  } else {
    print("You didn't win the lottery.");
  }
}
